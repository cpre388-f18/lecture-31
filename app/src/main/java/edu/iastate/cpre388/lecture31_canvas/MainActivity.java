package edu.iastate.cpre388.lecture31_canvas;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // TODO: uncomment the other line to see a SurfaceView
        setContentView(new CustomView(this));
//        setContentView(new CustomSurfaceView(this));
    }

    /**
     * A custom View that overrides onDraw() to make an animation loop.  This happens on the main
     * thread.
     */
    private static class CustomView extends View {
        /** X position for the line animation */
        private int x = 0;
        /** A paint style for the line animation */
        private Paint mRedPaint = new Paint();

        public CustomView(Context context) {
            super(context);

            // Set up a thick red brush
            mRedPaint.setColor(Color.RED);
            mRedPaint.setStrokeWidth(5);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            // Fill the view with cyan
            canvas.drawColor(Color.CYAN);

            // Draw a red line
            canvas.drawLine(0, 0, x, 100, mRedPaint);
            // Update the x position for an animation
            x = (x + 10) % getWidth();

            // Request that onDraw() be called again after processing other events
            invalidate();
        }
    }

    /**
     * A custom SurfaceView that uses a loop to continually refresh the animation.  This happens on
     * a separate thread.  Note that you can choose to lock the Canvas from many places.  You could
     * use an event loop or timer for a better design.
     */
    private static class CustomSurfaceView extends SurfaceView implements SurfaceHolder.Callback {
        /** A thread that has a loop to regularly draw an animation. */
        private Thread mAnimationThread;
        /** A thick blue paint to be used on the line */
        private Paint mRedPaint = new Paint();
        /** Used to signal that the thread should exit cleanly when the SurfaceView is destroyed */
        private boolean mRunning = true;
        /** The x position of the animation */
        private int x = 0;

        public CustomSurfaceView(Context context) {
            super(context);
            // The SurfaceHolder.Callback and SurfaceView may be different classes.  Here, I've made
            // one unified class that extends and implements each.
            getHolder().addCallback(this);

            // Initialize the paint.
            mRedPaint.setColor(Color.BLUE);
            mRedPaint.setStrokeWidth(5);
        }

        @Override
        public void surfaceCreated(final SurfaceHolder holder) {
            mRunning = true;
            // This is a quick and dirty way to create a thread with a loop for animating the line.
            mAnimationThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (mRunning) {
                        // NOTE: This code is simplified.  To do this properly, read the
                        // documentation and do appropriate null checking and such.

                        // A lock on a canvas & backing bitmap buffer.
                        Canvas c = holder.lockCanvas();
                        // Clear the bitmap buffer.
                        // TODO: comment out this line to see artifacts of double bufferring.
                        c.drawColor(Color.WHITE);

                        c.drawLine(0, 0, x, 100, mRedPaint);
                        // Move the line's position to create an animation.  (good practice is to
                        // use real time to calculate the animation progress)
                        x = (x + 10) % getWidth();

                        // Tell the system that we are done with the drawing and it may be displayed
                        holder.unlockCanvasAndPost(c);

                        // Just a generic 100 ms sleep to prevent the animation from going too fast.
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            // Start the above runnable in a separate thread.
            mAnimationThread.start();
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            // Signal to the thread that it's time to end.
            mRunning = false;
        }
    }
}
